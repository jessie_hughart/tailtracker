# TailTracker

TailTracker allows you to "track" the tail of a file.  Especially useful
for monitoring log files.  In addition you can highlight a regular expresion
and clear the screen without having to stop/clear/start the tracker again.

This app requires an ANSI enabled console.

To enable ANSI colors in windows command prompt follow the below instructions
Add the following registry key:

[HKEY_CURRENT_USER\Console]
"VirtualTerminalLevel"=dword:00000001

For more indepth howto: https://superuser.com/a/1300251



## Requirements
* Elixir 1.8.0

## Build Instructions
* mix deps.get
* mix escript.build

## Usage: tailtracker <file>[,<file>] [Options]

### example:
  escript tailtracker file.txt,file2.txt,file3.txt -c -h="(and|or|the)"  --colors="blue,red,green"

## Options:
    -c --cat        Show entire file before tracking the tail (like cat <filename>)
	-h --highlight  highlight regular espression <requires ANSI enabled terminal>
       --colors     list of comma seperated list of colors (ex: blue,red,green, or 0 through 255)

## In App Commands:
*  q-enter quits tailtracker
*  c-enter clears screen <note requires an ANSI enabled terminal>
*  l-enter Show list of files being tracked
*  p<num>-enter pauses logging the file with index <num> obtained from l
*  u<num>-enter unpauses logging the file with index <num> obtained from l, printing lines from where logging was paused
*  ue<num>-enter unpauses logging the file with index <num> obtained from l, but skips to the end of the file
*  ?-enter to show list of commands
