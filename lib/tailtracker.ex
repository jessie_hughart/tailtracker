defmodule Tailtracker do
  @identstr "   "

  def showcommands() do
    IO.write("In App Commands\n")
    IO.write(@identstr <> "press 'q - enter' to quit\n")
    IO.write(@identstr <> "press 'c - enter' to clear screen\n")
    IO.write(@identstr <> "press 'l - enter' to show files being tracked\n")

    IO.write(
      @identstr <>
        "press 'p<num> - enter' pauses logging the file with index <num> obtained from l\n"
    )

    IO.write(
      @identstr <>
        "press 'u<num> - enter' unpauses logging the file with index <num> obtained from l, printing lines from where logging was paused\n"
    )

    IO.write(
      @identstr <>
        "press 'ue<num> - enter' unpauses logging the file with index <num> obtained from l, but skips to the end of the file\n"
    )

    IO.write(@identstr <> "press '? - enter' to show list of commands\n")
  end

  def getCommand(watch_list, "c") do
    IO.write([IO.ANSI.home(), IO.ANSI.clear()])
    getCommand(watch_list, IO.getn(""))
  end

  def getCommand(watch_list, "l") do
    IO.write("Index: FileName\n")

    for {_pid, _file, file_name, index, color} <-
          Enum.sort(watch_list, fn {_, _, _, a, _}, {_, _, _, b, _} -> a <= b end) do
      IO.write(IO.ANSI.format([color, Integer.to_string(index), ": ", file_name, "\n", :normal]))
    end

    getCommand(watch_list, IO.getn(""))
  end

  def getCommand(watch_list, "p") do
    fd = IO.getn("")
    i = Integer.parse(fd)

    if i != :error do
      {index, _} = i
      rec = Enum.find(watch_list, &match?({_task, _file, _filename, ^index, _color}, &1))

      if rec != nil do
        IO.write("Pausing tracking on file: #{fd}\n")
        {task, _file, _filename, ^index, _color} = rec
        send(Map.get(task, :pid), :pause)
      end
    end

    getCommand(watch_list, IO.getn(""))
  end

  def getCommand(_watch_list, "q") do
    IO.puts("Quting")
  end

  def getCommand(watch_list, "u") do
    fd = IO.getn("")

    {type, fd} =
      if fd == "e" do
        {:unpause_eof, IO.getn("")}
      else
        {:unpause, fd}
      end

    i = Integer.parse(fd)

    if i != :error do
      {index, _} = i
      rec = Enum.find(watch_list, &match?({_task, _file, _filename, ^index, _color}, &1))

      if rec != nil do
        IO.write("UnPausing tracking on file: #{fd}\n")
        {task, _file, _filename, ^index, _color} = rec
        send(Map.get(task, :pid), type)
      end
    end

    getCommand(watch_list, IO.getn(""))
  end

  def getCommand(watch_list, "?") do
    showcommands()
    getCommand(watch_list, IO.getn(""))
  end

  def getCommand(watch_list, _) do
    getCommand(watch_list, IO.getn(""))
  end

  def readfile(file, opts, color) do
    str = IO.read(file, :line)

    if str != :eof do
      output =
        if Keyword.has_key?(opts, :highlight) == true do
          {:ok, regex} = Regex.compile(opts[:highlight], [:caseless])

          Regex.split(regex, str, include_captures: true, trim: true)
          |> Enum.map(fn x ->
            if Regex.match?(regex, x) do
              IO.ANSI.format([color, "", :yellow, :bright, x, :normal], true)
            else
              IO.ANSI.format([color, x, :normal], true)
            end
          end)
        else
          IO.ANSI.format([color, str, :normal], true)
        end

      IO.write(output)
      readfile(file, opts, color)
    end
  end

  def watchfile(file_name, file, opts, color, pause \\ nil) do
    receive do
      :pause ->
        watchfile(file_name, file, opts, color, true)

      :unpause ->
        watchfile(file_name, file, opts, color, nil)

      :unpause_eof ->
        goto_end_of_file(file_name, file)
        watchfile(file_name, file, opts, color, nil)
    after
      1_000 -> if pause != true, do: readfile(file, opts, color)
    end

    watchfile(file_name, file, opts, color, pause)
  end

  def goto_end_of_file(file_name, file) do
    {:ok, stats} = File.stat(file_name)
    size = Map.get(stats, :size)
    :file.position(file, size)
  end

  def main([]) do
    IO.puts("")
    IO.puts("Usage: tailtracker <file> [Options]")
    IO.puts("")
    IO.puts(@identstr <> "file tail watcher")
    IO.puts("")
    IO.puts(@identstr <> "escript tailtracker file.txt")
    IO.puts("")
    IO.puts("Options:")

    IO.puts(
      @identstr <>
        "-c --cat          Show entire file before tracking the tail (like cat <filename>)"
    )

    IO.puts(@identstr <> "-h --highlight    highlight regular espression")

    IO.puts(
      @identstr <>
        "   --colors       list of comma seperated list of colors (ex: blue,red,green or 0 through 255)"
    )

    IO.puts("")
    showcommands()
    IO.puts("")
  end

  def main([fname | args]) do
    {opts, _, _} =
      OptionParser.parse(args,
        strict: [cat: :boolean, highlight: :string, colors: :string],
        aliases: [c: :cat, h: :highlight]
      )

    valid_colors = [
      "normal",
      "green",
      "cyan",
      "blue",
      "magenta",
      "light_green",
      "light_cyan",
      "light_blue",
      "light_magenta",
      "black",
      "light_black",
      "yellow",
      "light_yellow",
      "white",
      "light_white",
      "red",
      "light_red"
    ]

    color_list =
      if Keyword.has_key?(opts, :colors) do
        Enum.reject(
          Stream.map(
            String.split(opts[:colors], ","),
            fn x ->
              cond do
                x in valid_colors ->
                  String.to_atom(x)

                match?({_, ""}, {num, _} = Integer.parse(x)) ->
                  if num >= 0 and num <= 255 do
                    IO.ANSI.color(num)
                  else
                    ""
                  end

                true ->
                  ""
              end
            end
          ),
          &(&1 == nil)
        )
      else
        [
          :normal,
          :green,
          :cyan,
          :blue,
          :magenta,
          :light_green,
          :light_cyan,
          :light_blue,
          :light_magenta
        ]
      end

    file_list = String.split(fname, ",")

    watch_list =
      for {{file_name, color}, index} <-
            Enum.with_index(Stream.zip(file_list, Stream.cycle(color_list))) do
        {status, file} = File.open(file_name)

        if status == :ok do
          IO.write(IO.ANSI.format([color, "tracking: #{file_name}\n", :normal]))

          if Keyword.has_key?(opts, :cat) == false do
            goto_end_of_file(file_name, file)
          end

          watchtask = Task.async(fn -> watchfile(file_name, file, opts, color) end)
          {watchtask, file, file_name, index, color}
        else
          IO.puts(IO.ANSI.format([:red, "Error reading file #{file_name}", :normal]))
        end
      end

    showcommands()

    # in the below statement we filter out :ok as this just means the comprehension finished.
    # the watch_list will contain a tuple with {pid, file} if was successful.
    # if we could not open any files, do not enter loop for commands
    if length(Enum.filter(watch_list, &(&1 != :ok))) > 0 do
      getCommand(Enum.filter(watch_list, &(&1 != :ok)), IO.getn(""))

      for {pid, file, _, _, _} <- watch_list do
        Task.shutdown(pid)
        File.close(file)
      end
    end
  end
end
